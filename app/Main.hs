module Main where

import           Control.Monad                  ( forM_ )
import           DCG                            ( Knowledge(..)
                                                , Mode(Generative, Selective)
                                                , allMembers
                                                , eval
                                                , isMember
                                                )
import           Data.Array.IO                  ( IOArray
                                                , getElems
                                                , newListArray
                                                , readArray
                                                , writeArray
                                                )
import qualified Entertainment                 as EN
                                                ( knowledge )
import           Options.Applicative            ( Parser
                                                , ParserInfo
                                                , auto
                                                , execParser
                                                , fullDesc
                                                , header
                                                , help
                                                , helper
                                                , info
                                                , long
                                                , metavar
                                                , option
                                                , progDesc
                                                , short
                                                , strOption
                                                , value
                                                )
import qualified Politics                      as PL
                                                ( knowledge )
import           System.Random                  ( randomRIO )


data Topic = Entertainment | Politics deriving (Eq, Read, Show)

data Opts = Opts
    { mode     :: Mode
    , sentence :: String
    , topic    :: Topic
    , limit    :: Int
    }


main :: IO ()
main = do
    opts <- execParser parserInfo
    case opts of
        (Opts Generative _ t l) -> do
            kwg <- shuffleKnowledge $ pickKnowledge t
            let sentences = take l $ eval <$> allMembers kwg
            mapM_ putStrLn sentences
        (Opts Selective s@(_ : _) t _) -> case isMember (pickKnowledge t) s of
            Just _  -> print "Valid news header."
            Nothing -> print "Invalid news header."
        (Opts Selective "" _ _) -> error "missing `sentence` for `Selective` mode"


parserInfo :: ParserInfo Opts
parserInfo = info (helper <*> optionParser)
                  (fullDesc <> header "header-generator" <> progDesc "A generator of news headers in spanish")

optionParser :: Parser Opts
optionParser =
    Opts
        <$> option auto (long "mode" <> short 'm' <> metavar "MODE" <> help "valid modes [Generative, Selective]")
        <*> strOption (long "sentence" <> short 's' <> value "" <> metavar "SENTENCE" <> help "sentence to validate")
        <*> option auto (long "topic" <> short 't' <> metavar "TOPIC" <> help "valid topics [Politics, Entertainment]")
        <*> option
                auto
                (long "limit" <> short 'l' <> value 10 <> metavar "LIMIT" <> help
                    "maximum number of headers to generate"
                )

pickKnowledge :: Topic -> Knowledge
pickKnowledge Entertainment = EN.knowledge
pickKnowledge Politics      = PL.knowledge

shuffleKnowledge :: Knowledge -> IO Knowledge
shuffleKnowledge k = do
    hs  <- shuffleList $ headers k
    ss  <- shuffleList $ subjects k
    as  <- shuffleList $ adverbs k
    ps  <- shuffleList $ prepositions k
    vs  <- shuffleList $ verbs k
    vos <- shuffleList $ verbObjects k
    pos <- shuffleList $ prepositionObjects k
    return $ Knowledge hs ss as ps vs vos pos

shuffleList :: [a] -> IO [a]
shuffleList xs = do
    let n = length xs
    arr <- newArray n xs
    forM_ [1 .. n] $ \i -> do
        j      <- randomRIO (i, n)
        valuei <- readArray arr i
        valuej <- readArray arr j
        writeArray arr i valuej
        writeArray arr j valuei
    getElems arr

newArray :: Int -> [a] -> IO (IOArray Int a)
newArray n xs = newListArray (1, n) xs
