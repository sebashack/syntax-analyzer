module Entertainment
    ( knowledge
    ) where

import           DCG                            ( Knowledge(..) )

knowledge :: Knowledge
knowledge = Knowledge { headers            = ["", "sorprendentemente", "lo mas de moda,", "lo nuevo,"]
                      , subjects           = ["Taylor Swift", "Katy Perry", "Justin Bieber", "Pharrell Williams"]
                      , verbs              = ["canta", "compone"]
                      , verbObjects        = ["", "la escena", "la cancion"]
                      , adverbs            = ["", "reveladoramente", "seductoramente"]
                      , prepositions       = ["en", "desde"]
                      , prepositionObjects = ["el concierto", "redes sociales"]
                      }
