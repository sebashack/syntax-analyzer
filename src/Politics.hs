module Politics
    ( knowledge
    ) where

import           DCG                            ( Knowledge(..) )

knowledge :: Knowledge
knowledge = Knowledge
    { headers            = ["", "ultima hora", "importante,"]
    , subjects           = [ "el presidente Duque"
                           , "el gobierno"
                           , "el ex-ministro de salud"
                           , "la ministra"
                           , "la camara de comercio"
                           ]
    , adverbs            = ["", "justamente", "descaradamente", "indiscriminadamente"]
    , prepositions       = ["en"]
    , verbs              = ["aprueba", "denuncia", "critica"]
    , verbObjects        = ["", "el patrimonio", "el banco", "un ministro"]
    , prepositionObjects = ["el pais", "el departamento de antioquia"]
    }
