{-# LANGUAGE GADTs #-}
{-# LANGUAGE TupleSections #-}

module DCG where

import           Control.Applicative            ( Alternative((<|>), empty) )
import           Data.Char                      ( toUpper )
import           Parser                         ( Parser(..)
                                                , symbol
                                                )


data Knowledge = Knowledge
    { headers            :: [String]
    , subjects           :: [String]
    , adverbs            :: [String]
    , prepositions       :: [String]
    , verbs              :: [String]
    , verbObjects        :: [String]
    , prepositionObjects :: [String]
    }

type Atom = String
type Phrase = Expr

data Expr where
  Sentence ::Expr -> Phrase -> Phrase -> Phrase -> Expr
  VerbPhrase ::Expr -> Expr -> Phrase
  VerbAdverbPhrase ::Phrase -> Expr -> Phrase
  PrepObjectPhrase ::Expr -> Expr -> Phrase
  Header::Atom -> Expr
  Subject::Atom -> Expr
  Preposition::Atom -> Expr
  Object ::Atom -> Expr
  Verb ::Atom -> Expr
  Adverb ::Atom -> Expr
  deriving Show

data Mode = Selective | Generative deriving (Eq, Read, Show)

eval :: Expr -> String
eval (Sentence e p1 p2 p3) =
    let capitalize ""       = ""
        capitalize (x : xs) = toUpper x : xs
        h  = eval e
        h' = if null h then capitalize (eval p1) else capitalize h <> " " <> eval p1
    in  h' <> " " <> eval p2 <> " " <> eval p3
eval (VerbPhrase e1 e2) =
    let obj  = eval e2
        obj' = if null obj then obj else " " <> obj
    in  eval e1 <> obj'
eval (VerbAdverbPhrase p e) =
    let obj  = eval e
        obj' = if null obj then obj else " " <> obj
    in  eval p <> obj'
eval (PrepObjectPhrase e1 e2) = eval e1 <> " " <> eval e2
eval (Header      h         ) = h
eval (Subject     s         ) = s
eval (Adverb      av        ) = av
eval (Preposition p         ) = p
eval (Object      o         ) = o
eval (Verb        v         ) = v

isMember :: Knowledge -> String -> Maybe Expr
isMember kg input = case (runParser $ sentence kg Selective) input of
    []           -> Nothing
    ((v, _) : _) -> Just v

allMembers :: Knowledge -> [Expr]
allMembers kg = fst <$> (runParser (sentence kg Generative) $ "#X")

sentence :: Knowledge -> Mode -> Parser Expr
sentence kg mode = do
    h   <- header (headers kg) mode
    s   <- subject (subjects kg) mode
    vap <- verbAdverbPhrase
    pop <- prepObjPhrase
    return $ Sentence h s vap pop
  where
    verbPhrase :: Parser Phrase
    verbPhrase = do
        v <- verb (verbs kg) mode
        o <- verbObjs (verbObjects kg) mode
        return $ VerbPhrase v o
    --
    prepObjPhrase :: Parser Phrase
    prepObjPhrase = do
        p <- preposition (prepositions kg) mode
        o <- prepObjects (prepositionObjects kg) mode
        return $ PrepObjectPhrase p o
    --
    verbAdverbPhrase :: Parser Phrase
    verbAdverbPhrase = do
        vp <- verbPhrase
        ad <- adverb (adverbs kg) mode
        return $ VerbAdverbPhrase vp ad

header :: [String] -> Mode -> Parser Expr
header hs mode = atom mode Header hs

subject :: [String] -> Mode -> Parser Expr
subject ss mode = atom mode Subject ss

verbObjs :: [String] -> Mode -> Parser Expr
verbObjs vos mode = atom mode Object vos

prepObjects :: [String] -> Mode -> Parser Expr
prepObjects pos mode = atom mode Object pos

verb :: [String] -> Mode -> Parser Expr
verb vs mode = atom mode Verb vs

adverb :: [String] -> Mode -> Parser Expr
adverb as mode = atom mode Adverb as

preposition :: [String] -> Mode -> Parser Expr
preposition ps mode = atom mode Preposition ps

atom :: Mode -> (String -> Expr) -> [String] -> Parser Expr
atom mode f atoms = case mode of
    Generative -> generativeParser
    Selective  -> selectiveParser
  where
    generativeParser = Parser $ \_ -> (, "#X") . f <$> atoms
    --
    selectiveParser  = foldr (\s p -> (f <$> symbol s) <|> p) empty atoms
