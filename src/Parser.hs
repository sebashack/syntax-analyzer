module Parser where

import           Control.Applicative            ( Alternative((<|>), empty)
                                                , many
                                                , some
                                                )
import           Control.Monad                  ( void )
import           Data.Char                      ( isAlpha
                                                , isAlphaNum
                                                , isDigit
                                                , isLower
                                                , isSpace
                                                , isUpper
                                                )


newtype Parser a = Parser { runParser :: String -> [(a, String)] }

instance Functor Parser where
    fmap f p = Parser $ \input -> g <$> (runParser p $ input) where g (a, s) = (f a, s)

instance Applicative Parser where
    pure v = Parser $ \s -> [(v, s)]
    (Parser fab) <*> (Parser ga) = Parser $ \input -> [ (f a, s) | (f, out) <- fab input, (a, s) <- ga out ]

instance Monad Parser where
    return = pure
    (Parser ga) >>= fp = Parser $ \input -> [ (b, s) | (a, out) <- (ga input), (b, s) <- runParser (fp a) out ]

instance Alternative Parser where
    empty = Parser (const [])
    (Parser p1) <|> (Parser p2) = Parser $ \input ->
        let result = p1 input
        in  case result of
                [] -> p2 input
                _  -> result

-- Parse a single character
item :: Parser Char
item = Parser f
  where
    f []       = []
    f (x : xs) = [(x, xs)]

-- Parse a single character that satisfies a predicate
satisfies :: (Char -> Bool) -> Parser Char
satisfies p = do
    x <- item
    if p x then return x else empty

-- Parse a single digit
digit :: Parser Char
digit = satisfies isDigit

-- Parse a single lower case char
lower :: Parser Char
lower = satisfies isLower

-- Parse a single upper case char
upper :: Parser Char
upper = satisfies isUpper

-- Parse a single alphabetic char
alphabetic :: Parser Char
alphabetic = satisfies isAlpha

-- Parse a single alphanumeric char
alphanum :: Parser Char
alphanum = satisfies isAlphaNum

-- Parse a single char that is equal to input
char :: Char -> Parser Char
char c = satisfies (== c)

-- Parse a given string
string :: String -> Parser String
string []       = pure []
string (x : xs) = do
    void $ char x
    void $ string xs
    return (x : xs)

-- Parse an identifier that starts with lowercase and
-- the rest of it is any alphanum sequence.
ident :: Parser String
ident = do
    x  <- lower
    xs <- many alphanum
    return (x : xs)

-- Parse a natural number, including 0
nat :: Parser Int
nat = do
    xs <- some digit
    return $ read xs

-- Identify spaces and ignore them
space :: Parser ()
space = many (satisfies isSpace) >> pure ()


-- Parse an integer
int :: Parser Int
int = let negative = char '-' >> (fmap negate nat) in negative <|> nat

--
-- Utilities defined in terms token
--

-- Ignore any space before and after target
token :: Parser a -> Parser a
token p = do
    space
    v <- p
    space
    return v

-- Parse a string stripping any back and leading white space
symbol :: String -> Parser String
symbol = token . string

-- Parse identifier stripping any back and leading white space
identifier :: Parser String
identifier = token ident

-- Parse natural number stripping any back and leading white space
natural :: Parser Int
natural = token nat

-- Parse integer stripping any back and leading white space
integer :: Parser Int
integer = token int

-- Parse a positive real number
positiveReal :: Parser Double
positiveReal = pointDecimal <|> intPointDecimal <|> intPoint <|> fromNatural
  where
    fromNatural = fromIntegral <$> natural
    --
    intPoint    = do
        r <- fromIntegral <$> natural
        void $ char '.'
        return r
    --
    pointDecimal = do
        void $ char '.'
        d <- natural
        let power = 10 ** (fromIntegral $ numDigits d)
        return $ fromIntegral d / power
    --
    intPointDecimal = do
        r <- fromIntegral <$> natural
        void $ char '.'
        d <- natural
        let power = 10 ** (fromIntegral $ numDigits d)
        if power > 0 then return $ r + (fromIntegral d / power) else return r
    --
    numDigits :: Int -> Int
    numDigits n = round (logBase (10 :: Double) (fromIntegral n)) + 1


-- Parse a real number
real :: Parser Double
real = let negative = char '-' >> (fmap negate $ token positiveReal) in negative <|> token positiveReal
