# syntax-analyzer

This code has only been tried out on Ubuntu20.04 and MacOS Catalina.


## First time setup

Run

```
./first-time-install.sh       -- Ubuntu
./first-time-install-macos.sh -- MacOS
```

## Build

Run

```
make build
```

## Run command line client

To generate news about politics, run a command like this:

```
stack exec header-generator -- -t Politics -m Generative -l 10
```

To generate news about entertainment, run a command like this:

```
stack exec header-generator -- -t Entertainment -m Generative -l 10
```

`-l` is the number of news headers you want would like to generate.


To check if a sentence is a valid production, run a command like this:

```
stack exec header-generator -- -t Entertainment -m Selective -s "Justin Bieber canta desde el concierto"
```
